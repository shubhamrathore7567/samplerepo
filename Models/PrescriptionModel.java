package Models;
import java.io.*;



import java.io.Serializable;


public class PrescriptionModel implements Serializable
{
    private int PrescriptionId;
    private String Medication;
    private String Reports;
    private int DoctorId;
    private int PatientId;
    public static int LastPrescriptionId = 0;

    
    public PrescriptionModel()
    {
        LastPrescriptionId++;
        this.setPrescriptionId(LastPrescriptionId);
    }

    
    public void setPrescriptionId(int prescriptionId){
        this.PrescriptionId = prescriptionId;
    }
    public int getPrescriptionId(){
        return PrescriptionId;    
    }

    public void setDoctorId(int DoctorId){
        this.DoctorId = DoctorId;
    }
    public int getDoctorId(){
        return DoctorId;    
    } 

    public void setPatientId(int PatientId)
	{
		this.PatientId = PatientId;
	}
	
	public int getPatientId()
	{
		return PatientId;
	}
	

    public void setMedication(String medication){
        this.Medication = medication;
    }

    public String getMedication(){
        return Medication;

    }

    public void setReports(String reports){
        this.Reports = reports;
    }

    public String getReports(){
        return Reports;

    }

}
