package Models;

import java.io.*;

// created by shivangi 9/03/2021
public class DoctorModel implements Serializable{
    private int DoctorId;
    private String Name;
    private String Speciality;
    private String Gender;
    private String PhoneNo;
    private String Address;
    private int Age;
    public static int LastDoctorID = 0;

    public DoctorModel()
    {
        LastDoctorID++;
        this.setDoctorId(LastDoctorID);
    }

    public void setDoctorId(int DoctorId){
        this.DoctorId = DoctorId;
    }
    public int getDoctorId(){
        return DoctorId;    
    } 

    public void setName(String name){
        this.Name = name;
    }
    public String getName(){
        return this.Name;
    }

    public void setSpeciality(String Speciality){
        this.Speciality = Speciality;
    }
    public String getSpeciality(){
        return Speciality;
    } 
    
    public void setGender(String Gender){
        this.Gender = Gender;
    }
    public String getGender(){
        return Gender;
    } 
    
    public void setPhoneNo(String PhoneNo){
        this.PhoneNo = PhoneNo;
    }
    public String getPhoneNo(){
        return PhoneNo;
    } 
    
    
    public void setAddress(String Address){
        this.Address = Address;
    }
    public String getAddress(){
        return Address;
    } 
    
    
    public void setAge(int age){
        this.Age = age;
    }
    public int getAge(){
        return this.Age;
    } 
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return (this.getDoctorId() == ((DoctorModel)obj).getDoctorId());
    }

    @Override
    public String toString() {
        return String.format("Doctor Data:\n Id : %d\n Name : %s\n Speciality : %s\n Gender : %s\n PhoneNo : %s\n Address : %s\n Age : %d",
                this.getDoctorId(),this.getName(),this.getSpeciality(),this.getGender(),this.getPhoneNo(),this.getAddress(),this.getAge());
    }

}
