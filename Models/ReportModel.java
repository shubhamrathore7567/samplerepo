// Created by Namjoshi on 11/3
package Models;

import java.io.*;

public class ReportModel implements Serializable{
    private int DoctorId;
    private int PatientId;
    private int ReportId;
    private String ReportAttachments;
    public static int LastReportId;
    //private String appointment_date;
	//private String status;
    
    public ReportModel()
    {
        this.setReportId(++LastReportId);
    }
    public void setDoctorId(int doctorId){
        this.DoctorId = doctorId;
    }
    public int getDoctorId(){
        return DoctorId;
    } 

    public void setPatientId(int patientId){
        this.PatientId = patientId;
    }
    public int getPatientId(){
        return PatientId;
    } 
    
    public void setReportId(int reportId){
        this.ReportId = reportId;
    }
    public int getReportId(){
        return ReportId;
    } 

    public void setReportAttachments(String attachments){
        this.ReportAttachments = attachments;
    }
    public String getReportAttachments(){
        return ReportAttachments;
    } 
    
        @Override
    public String toString() {
        return this.getReportId() +","+this.getDoctorId()+","+this.getPatientId()+","+ this.getReportAttachments();
    }
}
