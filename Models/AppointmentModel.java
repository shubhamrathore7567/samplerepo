package Models;
import java.io.*;

public class AppointmentModel implements Serializable{

    private int DoctorId;
    private int PatientId;
    private int AppointmentId;
    private String AppointmentTime;
    private String AppointmentDate ;
	private String Status;

    public static int LastAppointmentID = 0;

	public AppointmentModel()
	{
		LastAppointmentID++;
		setPatientId(LastAppointmentID);
	}

    
    public void setDoctorId(int doctorId){
        this.DoctorId = doctorId;
    }
    public int getDoctorId(){
        return this.DoctorId;
    } 

    public void setPatientId(int patientId){
        this.PatientId = patientId;
    }
    public int getPatientId(){
        return this.PatientId;
    } 
    
    public void setAppointmentId(int appointmentId){
        this.AppointmentId = appointmentId;
    }
    public int getAppointmentId(){
        return this.AppointmentId;
    } 

    public void setAppointmentTime(String appointmentTime){
        this.AppointmentTime = appointmentTime;
    }
    public String getAppointmentTime(){
        return this.AppointmentTime;
    } 
    
    public void setAppointmentDate(String appointmentDate){
        this.AppointmentDate = appointmentDate;
    }
    public String getAppointmentDate(){
        return this.AppointmentDate;
    } 
    
    public void setStatus(String status){
        this.Status = status;
    }
    public String getStatus(){
        return this.Status;
    } 
    
    
    
        @Override
    public String toString() {
        return this.getDoctorId()+","+this.getPatientId()+","+this.getAppointmentId() + "," + this.getAppointmentTime() + ":" + this.getAppointmentDate() + ":" + this.getStatus();
    }
}