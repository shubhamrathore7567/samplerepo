package Models;

import java.util.*;

public class Hospital {
    private String name;
    private List doctors;
    public void setName(String name)
    {
        this.name = name;
    }
    public String getName()
    {
        return this.name;
    }
    public void setDoctors(List<DoctorModel> doctors)
    {
        this.doctors = doctors;
    }
    public List<DoctorModel> getDoctors()
    {
        return this.doctors;
    }
}
