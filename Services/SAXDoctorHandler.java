package Services;

import Models.DoctorModel;
import Models.Hospital;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SAXDoctorHandler extends DefaultHandler {
    Hospital hospital = new Hospital();
    DoctorModel doctor = null;
    List doctorList = new ArrayList<>();
    String content = null;

    public void startElement(String namespaceURI,String localName,String qName,Attributes attributes)
    {
        if(qName.equals("Doctor"))
        {
            doctor = new DoctorModel();
        }
    }

    public void endElement(String namespaceURI,String localName,String qName)
    {
        switch(qName){
            case "Doctor" :  doctorList.add(doctor); break;
            case "DoctorId" : doctor.setDoctorId(Integer.parseInt(content)); break;
            case "Name" : doctor.setName(content); break;
            case "Speciality" : doctor.setSpeciality(content); break;
            case "Gender" : doctor.setGender(content); break;
            case "PhoneNo" : doctor.setPhoneNo(content); break;
            case "Address" : doctor.setAddress(content);break;
            case "Age" : doctor.setAge(Integer.parseInt(content));break;
        }
    }
    public void characters(char []ch,int start, int length)
    {
        content = new String(ch, start, length);
    }

    public List<DoctorModel> getDoctors()
    {
        return doctorList;
    }

    // public Hospital getHospital()
    // {
    //     return hospital;
    // }
}
