package Services;

import java.util.*;

import javax.print.Doc;

import java.io.*;
import Models.*;

public class GetterService {
    public void DisplayDoctors(List<DoctorModel> doctors)
    {
        for(DoctorModel doctor : doctors)
        {
            System.out.println(doctor);
        }
    }
    public void DisplayAppointment(List<AppointmentModel> appointments)
    {
        for(AppointmentModel appointment : appointments)
        {
            System.out.println(appointment);
        }
    }
}
