package Services;

import Models.AppointmentModel;


import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SAXAppointmentHandler extends DefaultHandler {
    AppointmentModel appointment = null;
    List appointmentList = new ArrayList<>();
    String content = null;

    public void startElement(String namespaceURI,String localName,String qName,Attributes attributes)
    {
        switch(qName){
            case "Appointment" :  appointmentList.add(appointment); break;
            case "AppointmentId" : appointment.setAppointmentId(Integer.parseInt(content)); break;
            case "DoctorId" : appointment.setDoctorId(Integer.parseInt(content)); break;
            case "PatientId" : appointment.setPatientId(Integer.parseInt(content)); break;
            case "AppointmentTime" : appointment.setAppointmentTime(content); break;
            case "AppointmentDate" : appointment.setAppointmentDate(content); break;
            case "Status" : appointment.setStatus(content);break;
        }
    }
    public void characters(char []ch,int start, int length)
    {
        content = new String(ch, start, length);
    }
    public List<AppointmentModel> getAppointmentList()
    {
        return appointmentList;
    }
}
