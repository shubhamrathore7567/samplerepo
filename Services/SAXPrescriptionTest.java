package Services;

import Models.PrescriptionModel;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import MoviesXML.SAXHandler;

public class SAXPrescriptionTest {
    public static void main(String[] args)
    {
        try
        {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            SAXPrescriptionHandler handler = new SAXPrescriptionHandler();
            Path path = Paths.get("PrescriptionData.xml");
            parser.parse(path.toFile(), handler);
            List<PrescriptionModel> prescriptions = handler.getPrescriptionList();
            for(PrescriptionModel prescription : prescriptions)
            {
                System.out.println(prescription);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
