package Services;

import Models.PatientModel;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SAXPatientHandler extends DefaultHandler {
    PatientModel patient = null;
    List patieentList = new ArrayList<>();
    String content = null;

    public void startElement(String namespaceURI,String localName,String qName,Attributes attributes)
    {
        switch(qName){
            case "Patient" : patieentList.add(patient); break;
            case "PatientId" : patient.setPatientId(Integer.parseInt(content)); break;
            case "Name" : patient.SetName(content); break;
            case "Age" : patient.setAge(Integer.parseInt(content)); break;
            case "Gender" : patient.setGender(content); break;
            case "PhoneNumber" : patient.setPhoneNo(content); break;
            case "Address" : patient.setAddress(content);break;
        }
    }
    public void characters(char []ch,int start, int length)
    {
        content = new String(ch, start, length);
    }
    public List<PatientModel> getPatientList()
    {
        return patieentList;
    }
}
