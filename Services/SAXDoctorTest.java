package Services;

import Models.DoctorModel;
import Models.Hospital;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXDoctorTest {
    public static void main(String[] args)
    {
        try
        {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            SAXDoctorHandler handler = new SAXDoctorHandler();
            Path path = Paths.get("DoctorData.xml");
            parser.parse(path.toFile(), handler);
            //Hospital hospital = handler.getHospital();
            List<DoctorModel> doctors = handler.getDoctors();
            for(DoctorModel doctor : doctors)
            {
                System.out.println(doctor);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
