package Services;

import Models.AppointmentModel;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class SAXAppointmentTest {
    public static void main(String[] args)
    {
        try
        {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            SAXAppointmentHandler handler = new SAXAppointmentHandler();
            Path path = Paths.get("AppointmentData.xml");
            parser.parse(path.toFile(), handler);
            List<AppointmentModel> appointments = handler.getAppointmentList();
            for(AppointmentModel appointment : appointments)
            {
                System.out.println(appointment);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
