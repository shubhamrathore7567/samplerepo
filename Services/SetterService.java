//Validation added by mohini

package Services;

import java.io.*;
import java.util.*;

import Models.DoctorModel;
import Models.PatientModel;
import Models.AppointmentModel;
import Models.PrescriptionModel;
import Models.ReportModel;

public class SetterService {
    
    public static Scanner sc = new Scanner(System.in);
    public static PatientModel SetPatient()
    {
        String PatientName,PatientGender,PatientAddress,PatientAge,PatientPhoneNo;
        try
        {
            PatientModel patient = new PatientModel();

            do
            {
                System.out.println("Enter Patient Name : ");
                PatientName = sc.nextLine();
            }while(!ValidateName(PatientName));
            patient.SetName(PatientName);

            do
            {
                System.out.println("Enter Gender : ");
                PatientGender = sc.nextLine();
            }while(!ValidateGender(PatientGender));
            patient.setGender(PatientGender);


            do
            {
                System.out.println("Enter Address : ");
                PatientAddress= sc.nextLine();
            }while(!ValidateAddress(PatientAddress));
            patient.setAddress(PatientAddress);


            do
            {
                System.out.println("Enter phoneNo : ");
                PatientPhoneNo= sc.nextLine();
            }while(!ValidatePhoneNo(PatientPhoneNo));
            patient.setPhoneNo(PatientPhoneNo);


            do
            {
                System.out.println("Enter Age : ");
                PatientAge= sc.nextLine();
            }while(!ValidateAge(PatientAge));
            patient.setAge(Integer.parseInt(PatientAge));

            return patient;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public static PrescriptionModel setPrescription()
    {
        try
        {
            PrescriptionModel prescription = new PrescriptionModel();
            BufferedReader inFromUSer = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter Doctor id : ");
            prescription.setDoctorId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Patient Id : ");
            prescription.setPatientId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Medication : ");
            prescription.setMedication(inFromUSer.readLine());
            System.out.println("Enter Report : ");
            prescription.setReports(inFromUSer.readLine());
             return prescription;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
    public static AppointmentModel SetAppointment(int patientId)
    {
        try
        {
            AppointmentModel Appointment = new AppointmentModel();
            BufferedReader inFromUSer = new BufferedReader(new InputStreamReader(System.in));
            Appointment.setPatientId(patientId);
            System.out.println("Enter Appointment Date : ");
            Appointment.setAppointmentDate(inFromUSer.readLine());
            System.out.println("Enter Doctor id : ");
            Appointment.setDoctorId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Appointment Time : ");
            Appointment.setAppointmentTime(inFromUSer.readLine());
            Appointment.setStatus("Requested");
            return Appointment;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
    public static ReportModel SetReport()
    {
        try
        {
            ReportModel report = new ReportModel();
            BufferedReader inFromUSer = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter Doctor id : ");
            report.setDoctorId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Patient Id : ");
            report.setPatientId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Report  Id : ");
            report.setReportId(Integer.parseInt(inFromUSer.readLine()));
            System.out.println("Enter Report Attachments : ");
            report.setReportAttachments(inFromUSer.readLine());
            return report;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
    public static DoctorModel SetDoctor()
    {
        String DoctorName,DoctorSpeciality,DorctoGender,DoctorPhoneNo,DoctorAddress;
        try
        {
            DoctorModel doctor = new DoctorModel();
            do
            {
                System.out.println("Enter Dooctor Name : ");
                DoctorName = sc.nextLine();
            } while(!ValidateName(DoctorName));
            doctor.setName(DoctorName);
            do
            {
                System.out.println("Enter Speciality : "); 
                DoctorSpeciality = sc.nextLine();
            }while(!ValidateSpeciality(DoctorSpeciality));
            doctor.setSpeciality(DoctorSpeciality);
            do
            {
                System.out.println("Enter Gender : "); // male female others
                DorctoGender =  sc.nextLine();
            }while(!ValidateGender(DorctoGender));
            doctor.setGender(DorctoGender);;
            
            do
            {
                System.out.println("Enter Phoneno : ");
                DoctorPhoneNo = sc.nextLine();
            }while(!ValidatePhoneNo(DoctorPhoneNo));
            doctor.setPhoneNo(DoctorPhoneNo); //regular expression


            do {
                System.out.println("Enter Address : ");
                DoctorAddress = sc.nextLine();
            } while (!ValidateAddress(DoctorAddress));
            System.out.println("Enter Age : ");
            doctor.setAge(Integer.parseInt(sc.nextLine()));
            return doctor;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public static String SetUsername()
    {
        try{
            System.out.println("Enter Name:");
            return sc.nextLine();
        }
        catch(Exception ex)
        {
            return "";
        }
    }

    public static String SetPhonoNo() {
        try{
            System.out.println("Enter PhoneNo:");
            return sc.nextLine();
        }
        catch(Exception ex)
        {
            return "";
        }
    }
    public static boolean ValidateName(String name)
    {
        if(!name.matches("[a-zA-Z_]+"))
		{
            System.out.println("Invalid name!");
            return false;
		}
		else 
        {
            return true;
        }
    }

    public static boolean ValidateSpeciality(String Speciality)
    {
        if(!Speciality.matches("[a-zA-Z_]+"))
		{
            System.out.println("Invalid String!");
            return false;
		}
        else
        {
            return true;
        }
    }

    public static boolean ValidateGender(String gender)
    {
        if(!gender.matches(".*\\bMale|Female|Others\\b.*"))
        {
            System.out.println("Invalid Gender!");
            return false;
        }
        else
        {
            return true;
        }
    }

    public static boolean ValidatePhoneNo(String Phoneno)
    {
        if(!Phoneno.matches("^\\d{10}$"))
        {
            System.out.println("Invalid Phone number!");
            return false;
        }
        else
        {
            return true;
        }
    }

    public static boolean ValidateAddress(String Address)
    {
        if(Address.length()<10) 
        {
            System.out.println("Address' minimum length should be 10");
            return false;
        }
        else
        {
            return true;
        }
    }

    public static boolean ValidateAge(String age)
    {
        if(!age.matches("\\d+"))
        {
            System.out.println("Age should be number only!");
            return false;
        }
        else
        {
            return true;
        }
    }
}
