package Services;

import Models.PatientModel;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class SAXPatientTest {
    public static void main(String[] args)
    {
        try
        {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            SAXPatientHandler handler = new SAXPatientHandler();
            Path path = Paths.get("PatientData.xml");
            parser.parse(path.toFile(), handler);
            List<PatientModel> patients = handler.getPatientList();
            for(PatientModel patient : patients)
            {
                System.out.println(patient);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
