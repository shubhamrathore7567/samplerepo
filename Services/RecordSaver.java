//Created by Namjoshi on 11/3/21...used to add records to all xml files
package Services;

import Models.PatientModel;
import Models.PrescriptionModel;
import Models.DoctorModel;
import Models.AppointmentModel;
import Models.ReportModel;


import java.io.*;


import javax.lang.model.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class RecordSaver
{
    public void AddRecord(PatientModel patient, DoctorModel doctor,AppointmentModel appointment, PrescriptionModel prescription,ReportModel report){
        
        String fileName = "";
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        System.out.println("In the Record ");
        if(patient!=null ){
            fileName = "PatientData.xml"; 
        }
        else if(doctor !=null)
        {
            fileName = "DoctorData.xml";
        }
        else if(appointment !=null)
        {
            fileName = "AppointmentData.xml";
        }
        else if(prescription !=null){
            fileName ="PrescriptionData.xml";
        }
        else{

            fileName ="ReportData.xml";
        }

        

        try{
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fileName);
            // add element to document
            Element rootElement = doc.getDocumentElement();
           
            if(fileName.equals("PatientData.xml"))
            {
                rootElement.appendChild(getPatient(doc, patient.getPatientId(),patient.GetName(),patient.getGender(),patient.getAddress(),patient.getAge(),patient.getPhoneNo()));
            }
            else if(fileName.equals("DoctorData.xml"))
            {
                rootElement.appendChild(getDoctor(doc,doctor.getDoctorId(),doctor.getName(),doctor.getSpeciality(),doctor.getGender(),doctor.getPhoneNo(),doctor.getAddress(),doctor.getAge()));
            }
            else  if(fileName.equals("AppointmentData.xml"))
            {
                rootElement.appendChild(getAppointment(doc,appointment.getAppointmentId(),appointment.getPatientId(),appointment.getDoctorId(),appointment.getAppointmentTime(),appointment.getAppointmentDate(),appointment.getStatus()));
            }
            else if(fileName.equals("PrescriptionData.xml"))
            {  
                rootElement.appendChild(getPrescription(doc,prescription.getPrescriptionId(), prescription.getDoctorId(),prescription.getPatientId(),prescription.getMedication(),prescription.getReports()));
            }
            else{

                rootElement.appendChild(getReport(doc,report.getReportId(),report.getDoctorId(),report.getPatientId(),report.getReportAttachments()));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            DOMSource source = new DOMSource(doc);

            System.out.println("In the Record - 2");
            //write to console or file
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(fileName));

            transformer.transform(source, console);
            transformer.transform(source,file);


        }catch(Exception e){
            e.printStackTrace();
        }

        
    }

    private static Node getPrescription(Document doc,int PrescriptionId,int DoctorId,int PatientId,  String Medication,String Reports){
        Element Prescription = doc.createElement("Prescription");

        // set id attribute
        Prescription.setAttribute("PrescriptionId", "" + PrescriptionId);

        //create age element
        Prescription.appendChild(getElement(doc, Prescription, "DoctorId", "" + DoctorId));

        //create role element
        Prescription.appendChild(getElement(doc, Prescription, "PatientId", "" + PatientId));

        //create gender element
        Prescription.appendChild(getElement(doc, Prescription, "Medication", Medication));

        //create PhoneNo element
        Prescription.appendChild(getElement(doc, Prescription, "Reports", Reports));

      
        return Prescription;
    }

    private static Node getDoctor(Document doc,int doctorid, String name,String speciality,String gender, String phoneno,String address,int age){
        Element Doctor = doc.createElement("Doctor");

        // set id attribute
        Doctor.setAttribute("DoctorId", "" + doctorid);
        Doctor.setAttribute("Name", "" + name);
        Doctor.setAttribute("PhoneNo", "" + phoneno);
        Doctor.appendChild(getElement(doc, Doctor, "DoctorId", "" + doctorid + ""));
        //create age element
        Doctor.appendChild(getElement(doc, Doctor, "Name", name));

        //create role element
        Doctor.appendChild(getElement(doc, Doctor, "Speciality", speciality));

        //create gender element
        Doctor.appendChild(getElement(doc, Doctor, "Gender", gender));

        //create PhoneNo element
        Doctor.appendChild(getElement(doc, Doctor, "PhoneNo", phoneno));

        //create address element
        Doctor.appendChild(getElement(doc, Doctor, "Address", address));

        //create address element
        Doctor.appendChild(getElement(doc, Doctor, "Age", "" + age + ""));

        return Doctor;
    }

    private static Node getPatient(Document doc,int patientid, String name,String gender,String address, int age, String phoneno){
        Element Patient = doc.createElement("Patient");
        Patient.setAttribute("Name", "" + name);
        Patient.setAttribute("PhoneNo", "" + phoneno);

        // set id attribute
        Patient.setAttribute("PatientId", "" + patientid);

        Patient.appendChild(getElement(doc, Patient, "PatientId",""+ patientid + ""));
        //create age element
        Patient.appendChild(getElement(doc, Patient, "Name", name));


        //create gender element
        Patient.appendChild(getElement(doc, Patient, "Gender", gender));

        
        //create address element
        Patient.appendChild(getElement(doc, Patient, "Address", address));
        
        //create age element
        Patient.appendChild(getElement(doc, Patient, "Age", ""+age));


        //create PhoneNo element
        Patient.appendChild(getElement(doc, Patient, "PhoneNo", phoneno));

        return Patient;
    }
    private static Node getAppointment(Document doc,int appointment_id,int doctor_id,int patient_id,String appointment_time,String appointment_date,String status){
        Element appointment = doc.createElement("Appointment");

        // set Appointment id attribute
        appointment.setAttribute("AppointmentID", "" + appointment_id);
        appointment.setAttribute("DoctorID", "" + doctor_id);
        appointment.setAttribute("PatientID", "" + patient_id);

        
        appointment.appendChild(getElement(doc, appointment, "AppointmentID", String.valueOf(appointment_id)));
        //create Doctor id element
        appointment.appendChild(getElement(doc, appointment, "DoctorID", String.valueOf(doctor_id)));


        //create Patient id element
        appointment.appendChild(getElement(doc, appointment, "PatientID", String.valueOf(patient_id)));

        
        //create Appointment time element
        appointment.appendChild(getElement(doc,appointment, "AppointmentTime", appointment_time));
        
        //create Appointment Date element
        appointment.appendChild(getElement(doc,appointment, "AppointmentDate", ""+appointment_date));


        //create status element
        appointment.appendChild(getElement(doc,appointment, "Status",status));

        return appointment;
    }

    private static Node getReport(Document doc,int reportId,int doctorId,int patientId,String reportAttachments){
        Element appointment = doc.createElement("Appointment");

        // set Report id attribute
        appointment.setAttribute("ReportID", "" + reportId);

        //create Doctor id element
        appointment.appendChild(getElement(doc, appointment, "DoctorID", String.valueOf(doctorId)));


        //create Patient id element
        appointment.appendChild(getElement(doc, appointment, "PatientID", String.valueOf(patientId)));

        
        //create Report Attachments element
        appointment.appendChild(getElement(doc,appointment, "ReportAttachments", reportAttachments));
        
        return appointment;
    }
 

    //utility method to  create text node
    private static Node getElement(Document doc,Element element,String name,String value){
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }
 
    
}