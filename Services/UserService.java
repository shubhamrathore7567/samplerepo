package Services;

import Models.*;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import javax.lang.model.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
public class UserService
{
    private RecordSaver recordSaver=new RecordSaver();
    public Boolean RegisterPatient(PatientModel patient)
    {
        try
        {
           
            FileInputStream file = new FileInputStream(new File("./PatientData.xml"));
           
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/Patients/Patient[@PhoneNo='" + patient.getPhoneNo() + "']/Name";
            String name = xPath.compile(expression).evaluate(xmlDocument);
            
            if(name != null && name != "")
            {
                return false;
            }   
            DoctorModel doctor = null;
            AppointmentModel appointment = null;

            PrescriptionModel prescription = null;
            ReportModel report=null;
            recordSaver.AddRecord(patient,doctor,appointment,prescription,report);


        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean RegisterDoctor(DoctorModel doctor)
    {
        try
        {
            
            FileInputStream file = new FileInputStream(new File("./DoctorData.xml"));
            
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            
           
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/Doctors/Doctor[@PhoneNo='" + doctor.getPhoneNo()+ "']/Name";
            String name = xPath.compile(expression).evaluate(xmlDocument);
            if(name != null && name != "")
            {
                return false;
            }
            //name ="dsh"
            PatientModel patient = null;
            AppointmentModel appointment=null;
            PrescriptionModel prescription = null;
            ReportModel report=null;
            recordSaver.AddRecord(patient, doctor,appointment,prescription,report);
            // append code of adding this record Patientdata registration
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public Boolean RequestAppointment(AppointmentModel appointment)
    {
        try
        {
            
            FileInputStream file = new FileInputStream(new File("AppointmentData.xml"));
            
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            
           
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/Appointments/Appointment[contains(@DoctorId,'" + appointment.getDoctorId()+ "') and contains(@AppointmentTime,'"+ appointment.getAppointmentTime() +"') and contains(@AppointmentDate,'"+ appointment.getAppointmentDate() +"')]/time";
            String name = xPath.compile(expression).evaluate(xmlDocument);
            if(name != null && name != "")
            {
                return false;
            }
            //name ="dsh"
            PatientModel patient = null;
            DoctorModel doctor = null;
            PrescriptionModel prescription = null;
            ReportModel report=null;
            recordSaver.AddRecord(patient, doctor,appointment,prescription,report);
            // append code of adding this record Patientdata registration
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    
    public Boolean setPrescription(PrescriptionModel prescription)
    {
        try
        {
            
            FileInputStream file = new FileInputStream(new File("PrescriptionData.xml"));
            
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            PatientModel patient = null;
            AppointmentModel appointment=null;
            DoctorModel doctorModel = null;
            ReportModel report=null;
            recordSaver.AddRecord(patient, doctorModel,appointment,prescription,report);           

            // append code of adding this record Patientdata registration

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    

    public Boolean setReport(ReportModel report)
    {
        try
        {
            
            FileInputStream file = new FileInputStream(new File("ReportData.xml"));
            
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            PatientModel patient = null;
            AppointmentModel appointment=null;
            DoctorModel doctorModel = null;
            PrescriptionModel prescription=null;
            recordSaver.AddRecord(patient, doctorModel,appointment,prescription,report);           

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    


    public int LoginUser(String name, String phoneNo)
    {
        int id=0;
        try
        {
            FileInputStream file = new FileInputStream(new File("PatientData.xml"));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/Patients/Patient[contains(@Name,'" + name +"') and contains(@PhoneNo,'" + phoneNo+ "')]/PatientId";
            expression = xPath.compile(expression).evaluate(xmlDocument);
            if(xPath.compile(expression).evaluate(xmlDocument) != null)
            {
                id = Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument));
                System.out.println("Patient id : "+ id);
                // call GetPatientProfile
            }
            else{
                file = new FileInputStream(new File("DoctorData.xml"));
                xmlDocument = builder.parse(file);
                xPath = XPathFactory.newInstance().newXPath();
                expression = "/Doctors/Doctor[contains(@Name,'" + name +"') and contains(@PhoneNo,'" + phoneNo+ "')]/DoctorId";
                System.out.println("Hello in doctor's login");
                if(xPath.compile(expression).evaluate(xmlDocument) == null)
                {
                    return 0;
                }
                id = Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument));
                System.out.println("Doctor id : "+ id);
                // call GetDoctorProfile(id)
            }
        }
        catch(Exception ex)
        {
            
            return 0;
        }
        return id;
    }

    // this function get patients details from xml file and set value in class and that class display data
    public PatientModel GetPatientProfile(int patientId)
    {
        try{
            
            System.out.println("in profile");
            PatientModel patient = new PatientModel();
            FileInputStream file = new FileInputStream(new File("PatientData.xml"));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();
            patient.setPatientId(patientId);
            String expression = "/Patients/Patient[contains(@PatientId,'" + patient.getPatientId() + "')]/Name";
            patient.SetName(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Patients/Patient[contains(@PatientId,'" + patient.getPatientId() + "')]/Gender";
            patient.setGender(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Patients/Patient[contains(@PatientId,'" + patient.getPatientId() + "')]/Address";
            patient.setAddress(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Patients/Patient[contains(@PatientId,'" + patient.getPatientId() + "')]/PhoneNo";
            patient.setPhoneNo(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Patients/Patient[contains(@PatientId,'" + patient.getPatientId() + "')]/Age";
            patient.setAge(Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument)));
            return patient;
        }
        catch(Exception ex)
        {
            return null;
        }
       
    }

    public AppointmentModel GetAppointmentDetails(int appointmentId)
    {
        try{
            AppointmentModel  appointment = new AppointmentModel ();
            FileInputStream file = new FileInputStream(new File("AppointmentData.xml"));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();
            appointment.setAppointmentId(appointmentId);
            String expression = "/Appointments/Appointment[contains(@AppointmentId,'" + appointment.getAppointmentId() + "')]/PatientId";
            appointment.setPatientId(Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument)));
            expression = "/Appointments/Appointment[contains(@AppointmentId,'" + appointment.getAppointmentId() + "')]/DoctorId";
            appointment.setDoctorId(Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument)));
            expression = "/Appointments/Appointment[contains(@AppointmentId,'" + appointment.getAppointmentId() + "')]/AppointmentTime";
            appointment.setAppointmentTime(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Appointments/Appointment[contains(@AppointmentId,'" + appointment.getAppointmentId() + "')]/AppointmentDate";
            appointment.setAppointmentDate(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Appointments/Appointment[contains(@AppointmentId,'" + appointment.getAppointmentId() + "')]/Status";
            appointment.setStatus(xPath.compile(expression).evaluate(xmlDocument));
            return appointment;
        }
        catch(Exception ex)
        {
            return null;
        }
       
    }

    // this function get patients details from xml file and set value in class and that class display data
    public DoctorModel GetDoctorProfile(int doctorId)
    {
        try{
            DoctorModel doctor = new DoctorModel();
            FileInputStream file = new FileInputStream(new File("DoctorData.xml"));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();
            doctor.setDoctorId(doctorId);
            String expression = "/Doctors/Doctor[contains(@PatientId,'" + doctor.getDoctorId() + "')]/Name";
            doctor.setName(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Doctors/Doctor[contains(@PatientId,'" + doctor.getDoctorId() + "')]/Gender";
            doctor.setGender(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Doctors/Doctor[contains(@PatientId,'" + doctor.getDoctorId() + "')]/Address";
            doctor.setAddress(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Doctors/Doctor[contains(@PatientId,'" + doctor.getDoctorId() + "')]/PhoneNo";
            doctor.setPhoneNo(xPath.compile(expression).evaluate(xmlDocument));
            expression = "/Doctors/Doctor[contains(@PatientId,'" + doctor.getDoctorId() + "')]/Age";
            doctor.setAge(Integer.parseInt(xPath.compile(expression).evaluate(xmlDocument)));
            return doctor;
        }
        catch(Exception ex)
        {
            return null;
        }
        
    }

    public List<DoctorModel> GetDoctorList()
    {
        try
        {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            SAXDoctorHandler handler = new SAXDoctorHandler();
            Path path = Paths.get("DoctorData.xml");
            parser.parse(path.toFile(), handler);
            //Hospital hospital = handler.getHospital();
            List<DoctorModel> doctors = handler.getDoctors();
            return doctors;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
}
