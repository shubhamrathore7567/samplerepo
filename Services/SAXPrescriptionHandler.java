package Services;

import Models.PrescriptionModel;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SAXPrescriptionHandler extends DefaultHandler {
    PrescriptionModel prescription = null;
    List prescriptionlist = new ArrayList<>();
    String content = null;

    public void startElement(String namespaceURI,String localName,String qName,Attributes attributes)
    {
        switch(qName){
            case "Prescription" :  prescriptionlist.add(prescription); break;
            case "PrescriptionId" : prescription.setPrescriptionId(Integer.parseInt(content)); break;
            case "Reports" : prescription.setReports(content); break;
            case "Medication" : prescription.setMedication(content); break;
            case "DoctorId" : prescription.setDoctorId(Integer.parseInt(content)); break;
            case "PatientId" : prescription.setPatientId(Integer.parseInt(content)); break;
        }
    }
    public void characters(char []ch,int start, int length)
    {
        content = new String(ch, start, length);
    }

    public List<PrescriptionModel> getPrescriptionList()
    {
        return prescriptionlist;
    }
}
