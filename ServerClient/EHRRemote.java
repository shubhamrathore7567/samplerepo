package Server;

import java.rmi.*;
import java.rmi.server.*;
import Models.*;
import Services.UserService;
import java.util.*;

public class EHRRemote extends UnicastRemoteObject implements RemoteStub
{
    UserService service = new UserService();
    public EHRRemote() throws RemoteException
    {
        super();
    }
    
    public boolean SetDoctor(DoctorModel model)
    {
        return service.RegisterDoctor(model);
    }

    public int LoginUser(String userName, String phoneNo)
    {
        return service.LoginUser(userName, phoneNo);
    }
    public boolean SetPatient(PatientModel model)
    {
        return service.RegisterPatient(model);
    }

    public boolean SetAppointment(AppointmentModel model) 
    {
        return service.RequestAppointment(model);
    }
     
    public boolean setPrescription(PrescriptionModel model)
    {
        return service.setPrescription(model);
    }
    
    public boolean SetReport(ReportModel model){
        return service.setReport(model);
    }

    public PatientModel ViewPatientProfile(int patientId)
    {
        return service.GetPatientProfile(patientId);
    }

    public DoctorModel ViewDoctorProfile(int doctorId)
    {
        return service.GetDoctorProfile(doctorId);
    }
    
    public List<DoctorModel> ViewDoctorList()
    {
        return service.GetDoctorList();
    }

    public Boolean RequestAppointment(AppointmentModel appointment)
    {
        return service.RequestAppointment(appointment);
    }
}
