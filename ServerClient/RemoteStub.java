package Server;

import java.rmi.*;
import Models.*;
import Services.*;
import java.util.List;

public interface RemoteStub extends Remote
{
    public boolean SetDoctor(DoctorModel model) throws RemoteException;

    public int LoginUser(String userName, String phoneNo) throws RemoteException;

    public boolean SetPatient(PatientModel model) throws RemoteException;

    public boolean SetAppointment(AppointmentModel model) throws RemoteException;
    
    public boolean SetReport(ReportModel model) throws RemoteException;

    public boolean setPrescription(PrescriptionModel model) throws RemoteException;

    public PatientModel ViewPatientProfile(int patientId) throws RemoteException;
    
    public DoctorModel ViewDoctorProfile(int doctorId) throws RemoteException;

    public List<DoctorModel> ViewDoctorList() throws RemoteException;

    public Boolean RequestAppointment(AppointmentModel appointment) throws RemoteException;
}