//namjoshi
//validation chnges made by mohini
package Server;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.ClosedFileSystemException;
import java.rmi.*;
import java.util.*;

import Services.GetterService;
import Services.SetterService;
import Models.*;


public class Client {
    public static void main(String[] args)throws Exception
    {
        BufferedReader inFromUSer = new BufferedReader(new InputStreamReader(System.in));
        String choice = "";
        System.out.println("Hello Welcome to EHRS(Created by Sinceres)");
        SetterService _setter = new SetterService();
        GetterService _getter = new GetterService();
        int status = 0;      
        boolean registered = false;

        RemoteStub stub = (RemoteStub) Naming.lookup("rmi://localhost:5000/mcaiv");
        while(!choice.equals("no")) // choice = yes
        {
            
            if(status==0) // if not logged in
            {
                if(!registered && status == 0)
                {
                    System.out.println("Do you have your account in our system?");
                    choice = inFromUSer.readLine();
                }
                else
                {
                    System.out.println("Please login!");
                    choice = "yes";
                }
                if(choice.toLowerCase().equals("yes"))
                {
                    System.out.println("----------------------------------");
                    System.out.println("\t\tLogin  ");
                    System.out.println("----------------------------------");

                    status = stub.LoginUser(_setter.SetUsername(),_setter.SetPhonoNo());//status will be reset 
                    if(status == 0) // will not get id 
                    {
                        System.out.println("Invalid credentials! please check username or phone Number");
                    }
                    else //get id 
                    {
                        System.out.println("You have successfully logged in " + status);
                        // check the login id of patient or doctor
                    }
                }
                else if(choice.toLowerCase().equals("no"))
                {
                    System.out.println("Do you want to register as Patient/Doctor?");
                    choice = inFromUSer.readLine();
                    if(choice.toLowerCase().equals("doctor"))
                    {
                        if(stub.SetDoctor(_setter.SetDoctor()))
                        {
                            System.out.println("You are successfully registered as doctor.");
                            registered = true;
                        }
                        else{
                            System.out.println("There is some issue with your data please check your phone no has already registered or not by login.");
                        }
                    }
                    else if(choice.toLowerCase().equals("patient"))
                    {
                        if(stub.SetPatient(_setter.SetPatient()))
                        {
                            System.out.println("You are successfully registered as patient.");
                            registered = true;
                        }
                        else{
                            System.out.println("There is some issue with your data please check your phone no has already registered or not by login.");
                        }
                    }
                }
            }
            else
            {
                PatientModel patient = stub.ViewPatientProfile(status);
                System.out.println(patient);
                if(patient != null)
                {
                    System.out.println("What you want to do now(View appointments/View Doctors/Request appointment/View Reports/View Priscriptions)");
                    while(!choice.toLowerCase().equals("no"))
                    {
                        System.out.println("Enter your choice :");
                        choice = inFromUSer.readLine();
                        switch(choice)
                        {
                            case "View appointments" : break;
                            case "Request appointment" : stub.RequestAppointment(_setter.SetAppointment(status));  break;
                            case "View Reports" : break;
                            case "View Priscriptions" : break;
                            case "View Doctors": _getter.DisplayDoctors(stub.ViewDoctorList()); break;
                            default : System.out.println("Wrong choice"); 
                        }
                    }
                    if(choice.toLowerCase().equals("no"))
                    {
                        break;
                    }
                }
                else
                {
                    DoctorModel doctor = stub.ViewDoctorProfile(status);
                    if(doctor != null)
                    {
                        System.out.println("What you want to do now(View appointments/Approve appointment/View Reports/View Priscriptions/Send Priscriptions/Send Reports)");
                        while(!choice.toLowerCase().equals("no"))
                        {
                            System.out.println("Enter your choice :");
                            choice = inFromUSer.readLine();
                        }
                        if(choice.toLowerCase().equals("no"))
                        {
                            break;
                        }
                    }
                }
            }
            System.out.println("Do you want to continue for further process(Yes or No)?");

            choice = inFromUSer.readLine();
        }
        System.out.println("Thanks for visit, bye!");
    }
}
